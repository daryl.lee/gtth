module gtth

go 1.14

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/contrib v0.0.0-20200913005814-1c32036e7ea4
	github.com/gin-gonic/gin v1.6.3
	github.com/go-test/deep v1.0.7
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/onsi/ginkgo v1.14.1
	github.com/onsi/gomega v1.10.2
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
)
