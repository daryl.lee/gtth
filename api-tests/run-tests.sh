#!/usr/bin/env bash

npm install -g newman-reporter-html

RESULTS_FILE=/etc/newman-results/results.html

newman run /etc/newman/gtth.tc.json --environment=/etc/newman/gtth.env.json --reporters html,cli --reporter-html-export ${RESULTS_FILE}


num_fail=$(grep 'Total Failures' ${RESULTS_FILE} | sed 's#.*Total Failures.*<strong>\(.*\)<\/strong>.*#\1#')
if [ $num_fail -ne 0 ]; then
    echo "notifying slack"
    echo "${num_fail} Test Failed!"
    curl -X POST -H 'Content-type: application/json' --data '{"text":"'${num_fail}' Test Failed!"}' https://hooks.slack.com/services/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
    exit 1
else
    echo "All Test Passed!"
    curl -X POST -H 'Content-type: application/json' --data '{"text":"All Test Passed!"}' https://hooks.slack.com/services/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
    exit 0
fi
