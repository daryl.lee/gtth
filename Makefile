build:
	GO111MODULE=on go build -mod vendor -o ./bin/main ./api

run:
	cd api; GO111MODULE=on go run .

test:
	GO111MODULE=on go test ./api/...

dep:
	GO111MODULE=on go mod vendor


build_image:
	docker build -f ./deployments/build/Dockerfile -t api:latest .

tar_image: 
	docker save -o ./img/api.tar api:latest

untar_image:
	docker load -i ./img/api.tar
		
deploy_image:
	echo "TBD: deploy_image"

test_image:
	docker-compose -f ./deployments/docker-compose.yml up -V --exit-code-from api_tester
