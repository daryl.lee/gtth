package model

import (
	"github.com/jinzhu/gorm"
	logger "github.com/sirupsen/logrus"
	"strings"
	_ "gtth/api/log"
)

type Employee struct {
	//Id     uint64 `gorm:"primary_key;auto_increment" json:"id"`
	Id     int     `gorm:"primary_key;auto_increment" json:"id"`
	Login  string  `gorm:"size:32;unique;not null" json:"login"`
	Name   string  `gorm:"size:128;not null;default:'user'" json:"name"`
	Salary float64 `sql:"type:decimal(10,2);" json:"salary"`

	//UpdatedAt time.Time `gorm:"not null;default:CURRENT_TIMESTAMP" json:"updated_at"`
}

//func (s *Server) MassCreateEmployee(reader *csv.Reader) error {
func (s *Server) MassCreateEmployee(employeeList *[]Employee) error {

	// Create transactional writes
	txErr := s.DB.Transaction(func(tx *gorm.DB) error {

		for _, employee := range *employeeList {

			if err := tx.Create(&Employee{Id: employee.Id, Login: employee.Login, Name: employee.Name, Salary: employee.Salary}).Error; err != nil {
				return err // rollback
			}

			logger.Infof("updated")
		}
		return nil
	})
	if txErr != nil {
		return txErr
	}
	return nil
}

func (s *Server) GetEmployee(id uint64) (*Employee, error) {
	u := &Employee{}
	err := s.DB.Debug().Where("id = ?", id).First(&u).Error
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (s *Server) GetEmployeeById(id int) (*Employee, error) {
	u := &Employee{}
	err := s.DB.Debug().Where("id = ?", id).First(&u).Error
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (s *Server) GetEmployeeByName(name string) (*Employee, error) {
	u := &Employee{}
	err := s.DB.Debug().Where("lower(name) = ?", strings.ToLower(name)).First(&u).Error
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (s *Server) GetAllEmployee(minSalary string, maxSalary string, offset string, limit string, sortField string, sortType string) ([]Employee, error) {
	var u []Employee
	err := s.DB.Debug().
		Where("salary <= ?", maxSalary).
		Where("? <= salary", minSalary).
		Order(sortField + " " + sortType).
		Offset(offset).
		Limit(limit).
		Find(&u).
		Error
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (s *Server) DeleteAllEmployee() (error) {
	err := s.DB.Exec("delete from employees").Error
	if err != nil {
		return err
	}
	return nil
}
func (s *Server) CreateEmployee(u *Employee) (*Employee, error) {
	err := s.DB.Debug().Create(&u).Error
	if err != nil {
		logger.Errorf("create staff KO")
		return nil, err
	}
	return u, nil
}

func (s *Server) UpdateEmployee(u *Employee) (*Employee, error) {
	var ucheck Employee
	if err := s.DB.Where("id = ?", u.Id).First(&ucheck).Error; err != nil {
		return nil, err
	}

	err := s.DB.Debug().Save(&u).Error
	if err != nil {
		logger.Error("update staff KO")
		return nil, err
	}
	return u, nil
}

func (s *Server) DeleteEmployee(id uint64) (*Employee, error) {
	var u Employee
	err := s.DB.Debug().Where("id = ?", id).First(&u).Error
	if err != nil {
		return nil, err
	}

	//clear password record

	//clear staff record
	err = s.DB.Debug().Delete(&u).Error
	if err != nil {
		logger.Errorf("delete staff KO:%v:::%v", u, err.Error())
		return nil, err
	}
	return &u, nil
}
