package model

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //postgres database driver
)

type Server struct {
	DB *gorm.DB
}

var (
	Model ModelInterface = &Server{}
)

type ModelInterface interface {
	Initialize(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) (*gorm.DB, error)

	//MassCreateEmployee(reader *csv.Reader) error
	MassCreateEmployee(employeeList *[]Employee) error

	CreateEmployee(*Employee) (*Employee, error)
	UpdateEmployee(*Employee) (*Employee, error)
	GetEmployee(uint64) (*Employee, error)
	GetEmployeeById(id int) (*Employee, error)
	GetEmployeeByName(name string) (*Employee, error)
	GetAllEmployee(minSalary string, maxSalary string, offset string, limit string, sortField string, sortType string) ([]Employee, error)
	DeleteAllEmployee() error

	//GetAllUsersDevices(*[]User) ([]User, error)
	//GetUserByEmail(string) (*User, error)
	//GetUserByPhoneNumber(string) (*User, error)
	DeleteEmployee(id uint64) (*Employee, error)
}

func (s *Server) Initialize(dbDriver, dbUser, dbPassword, dbPort, dbHost, dbName string) (*gorm.DB, error) {
	var err error
	dbUrl := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", dbHost, dbPort, dbUser, dbName, dbPassword)
	s.DB, err = gorm.Open(dbDriver, dbUrl)
	if err != nil {
		return nil, err
	}
	s.DB.Debug().AutoMigrate(
		&Employee{},
	)
	return s.DB, nil
}

func CreateRepository(db *gorm.DB) ModelInterface {
	return &Server{
		DB: db,
	}
}
