package app

import (
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
	"log"
	"os"

	logger "github.com/sirupsen/logrus"
	"gtth/api/controller"
	_ "gtth/api/log"
	"gtth/api/model"
)

var DB *gorm.DB

func init() {
	var err error
	//err = godotenv.Load()
	//if err != nil {
	//	logger.Fatalf("Error getting env, %v", err)
	//}

	dbdriver := "postgres"
	dbPort := "5432"

	//dbdriver := ViperEnvVariable("DB_DRIVER")
	//username := ViperEnvVariable("DB_USER")
	//password := ViperEnvVariable("DB_PASSWORD")
	//dbHost := ViperEnvVariable("DB_HOST")
	//dbName := ViperEnvVariable("DB_NAME")
	//dbPort := ViperEnvVariable("DB_PORT")

	//dbdriver := os.Getenv("DB_DRIVER")
	username := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	dbHost := os.Getenv("DB_HOST")
	dbName := os.Getenv("DB_NAME")
	//dbPort := os.Getenv("DB_PORT")

	DB, err = model.Model.Initialize(dbdriver, username, password, dbPort, dbHost, dbName)
	if err != nil {
		logger.Fatalf("Error connecting to the username:%v password:%v dbPort:%v dbHost:%v dbName:%v:::%v",
			username, password, dbPort, dbHost, dbName, err)
	}

}

func ViperEnvVariable(key string) string {

	viper.SetConfigFile(".env")
	//viper.SetConfigFile("/Users/lf/dev-go/src/gtth/api/.env")

	// Find and read the config file
	err := viper.ReadInConfig()

	if err != nil {
		log.Fatalf("Error while reading config file %s", err)
	}

	// viper.Get() returns an empty interface{}
	// to get the underlying type of the key,
	// we have to do the type assertion, we know the underlying value is string
	// if we type assert to other type it will throw an error
	value, ok := viper.Get(key).(string)

	// If the type is a string then ok will be true
	// ok will make sure the program not break
	if !ok {
		log.Fatalf("Invalid type assertion")
	}

	return value
}

func StartApp() {
	logger.Warnf("app started")

	router := controller.SetupRouter()

	port := ViperEnvVariable("PORT")
	//port := os.Getenv("PORT")
	if port == "" {
		port = "9988"
	}
	host := ViperEnvVariable("HOST")
	//host := os.Getenv("HOST")
	if host == "" {
		host = "0.0.0.0"
	}
	logger.Fatal(router.Run(host + ":" + port))
}
