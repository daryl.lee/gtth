package controller

import (
	//"compress/gzip"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/gzip"
	"github.com/gin-gonic/gin"
	"net/http"

)

func SetupRouter() *gin.Engine {
	var router = gin.Default()

	router.Use(
		cors.New(cors.Config{
			AllowOrigins: []string{"*"},
			AllowMethods: []string{"GET", "PUT", "POST", "DELETE"},
			AllowHeaders: []string{"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, X-Max"},
		}),
		gzip.Gzip(gzip.DefaultCompression),
	)

	router.POST("/users/upload", PostUsersUpload)
	router.GET("/users", GetAllUsers)
	router.DELETE("/users", DeleteAllUsers)

	router.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"status": "ok",
		})
	})

	return router
}
