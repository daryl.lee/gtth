package controller

import (
	"github.com/gin-gonic/gin"
	logger "github.com/sirupsen/logrus"
	"net/http"
	"gtth/api/model"
)

func GetAllUsers(c *gin.Context) {

	minSalary := c.Query("minSalary")
	maxSalary := c.Query("maxSalary")
	offset := c.Query("offset")
	limit := c.Query("limit")
	sort := c.Query("sort")
	sortType := "asc"
	if string(sort[0]) == "-" {
		sortType = "desc"
	}
	sortField := sort[1:]

	logger.Infof("salary %v to %v   offset:%v   limit:%v   sort:%v   asc?%v", minSalary, maxSalary, offset, limit, sortField, sortType)

	employeeList, err := model.Model.GetAllEmployee(minSalary, maxSalary, offset, limit, sortField, sortType)
	if err != nil {
		c.JSON(http.StatusBadRequest, err.Error())
		return

	}

	c.JSON(http.StatusOK, gin.H{"results": employeeList})
	return
}

func DeleteAllUsers(c *gin.Context) {

	if err := model.Model.DeleteAllEmployee(); err != nil {
		logger.Errorf("delete employee KO:::%v", err)
		c.JSON(http.StatusBadRequest, "")
		return
	}
	c.JSON(http.StatusOK, "")
	return
}
