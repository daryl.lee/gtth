package controller

import (
	"encoding/csv"
	"fmt"
	"github.com/gin-gonic/gin"
	logger "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"strconv"
	"strings"
	"gtth/api/component"
	"gtth/api/model"
)

const EXPECTED_NUM_FIELDS = 4

var isDoingUpload = false

func PostUsersUpload(c *gin.Context) {
	if isDoingUpload {
		logger.Errorf("only 1 upload at a time")
		c.JSON(http.StatusBadRequest, "")
		return
	}
	isDoingUpload = true

	//time.Sleep(5 * time.Second)

	file, header, err := c.Request.FormFile("file")
	if err != nil {
		resp := component.RestResponse{
			Code:    component.IMPORT_USER_ERROR_MSG,
			Status:  component.REST_RESPONSE_STATUS_KO,
			Message: "read file error",
			Error:   err.Error(),
		}
		c.JSON(http.StatusBadRequest, resp)
		isDoingUpload = false
		return
	}
	filename := header.Filename
	fmt.Printf(filename)

	r := csv.NewReader(file)

	var employeeList []model.Employee
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			logger.Errorf(err.Error())
		}

		tmpId := strings.TrimSpace(record[0])
		if string(tmpId[0]) == "#" {
			logger.Infof("commented %v", tmpId)
			continue
		}
		newId, err := strconv.Atoi(tmpId)
		if err != nil {
			resp := component.RestResponse{
				Code:    component.IMPORT_USER_ERROR_MSG,
				Status:  component.REST_RESPONSE_STATUS_KO,
				Message: "invalid employee id",
				Error:   err.Error(),
			}
			logger.Errorf("%v", err.Error())
			c.JSON(http.StatusBadRequest, resp)
			isDoingUpload = false
			return
		}

		if len(record) != EXPECTED_NUM_FIELDS {
			logger.Errorf("num of fields:%v expected %v", len(record), EXPECTED_NUM_FIELDS)
			c.JSON(http.StatusBadRequest, "wrong num of fields")
			isDoingUpload = false
			return
		}

		newLogin := record[1]
		newName := record[2]
		newSalary, err := strconv.ParseFloat(record[3], 64)
		if err != nil {
			c.JSON(http.StatusBadRequest, "TBD")
			isDoingUpload = false
			return
		}
		if newSalary < 0.0 {
			logger.Errorf("negative salary %v", newSalary)
			c.JSON(http.StatusBadRequest, "negative salary")
			isDoingUpload = false
			return
		}

		employeeList = append(employeeList, model.Employee{Id: newId,
			Login:  newLogin,
			Name:   newName,
			Salary: newSalary,
		})
	}
	err = model.Model.MassCreateEmployee(&employeeList)

	if err != nil {
		resp := component.RestResponse{
			Code:    component.IMPORT_USER_ERROR_MSG,
			Status:  component.REST_RESPONSE_STATUS_KO,
			Message: "upload employee to db error",
			Error:   err.Error(),
		}
		logger.Errorf("%v", err.Error())
		c.JSON(http.StatusBadRequest, resp)
		isDoingUpload = false
		return
	}

	isDoingUpload = false
}
