package log

import (
	"fmt"
	"github.com/joho/godotenv"
	logger "github.com/sirupsen/logrus"
	"log"
	"os"
	"runtime"
	"strings"
)

func init() {

	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatalf("godotenv ko:::%v", err)
	}

	logger.SetReportCaller(true)

	lvl, ok := os.LookupEnv("LOG_LEVEL")
	if !ok {
		lvl = "debug"
	}
	ll, err := logger.ParseLevel(lvl)
	if err != nil {
		ll = logger.DebugLevel
	}
	logger.SetLevel(ll)

	formatter := &logger.TextFormatter{
		TimestampFormat:        "2006-01-02 15:04:05", // the "time" field configuratiom
		FullTimestamp:          true,
		DisableLevelTruncation: true, // log level field configuration
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			return "", fmt.Sprintf("%s:%d", formatFilePath(f.File), f.Line)
		},
	}
	logger.SetFormatter(formatter)
}
func formatFilePath(path string) string {
	arr := strings.Split(path, "/")
	return arr[len(arr)-1]
}
