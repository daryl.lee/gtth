package component

const (
	REST_RESPONSE_STATUS_OK = "ok"
	REST_RESPONSE_STATUS_KO = "failed"
)

const (
	IMPORT_USER_ERROR_MSG    = 20001
)

type RestResponse struct {
	Code    int         `json:"code"`    // from above
	Status  string      `json:"status"`  // from REST_RESPONSE_STATUS_XX
	Message string      `json:"message"` //for display to end user
	Error   interface{} `json:"error"`   //for front end debug
	Data    interface{} `json:"data"`
}

